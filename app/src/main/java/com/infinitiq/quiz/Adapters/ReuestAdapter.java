package com.infinitiq.quiz.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.infinitiq.quiz.R;
import com.infinitiq.quiz.activity.ProfileActivity;
import com.infinitiq.quiz.helper.AppController;
import com.infinitiq.quiz.helper.CircleImageView;
import com.infinitiq.quiz.helper.GetData;
import com.infinitiq.quiz.helper.Refresh;
import com.infinitiq.quiz.helper.RetrofitInstance;
import com.infinitiq.quiz.model.UserDetails;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReuestAdapter extends RecyclerView.Adapter<ReuestAdapter.ViewHolder> {

    ArrayList<UserDetails> st;
    Context ctx;

    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    Refresh refresh;
    int i;
    public ReuestAdapter(ArrayList<UserDetails> st, Context ctx,Refresh refresh,int i) {
        this.st = st;
        this.ctx = ctx;
        this.refresh = refresh;
        this.i = i;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_request,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        holder.setIsRecyclable(false);

        holder.imgProfile.setImageUrl("https://plotlelo.in/uploads/profile/"+st.get(position).getImage(), imageLoader);

//        Picasso.get().load("https://plotlelo.in/uploads/profile/"+st.get(position).getImage()).placeholder(R.drawable.ic_account).into(holder.imgProfile);
        holder.tvName.setText(st.get(position).getName());


        if(i==0){
            holder.btn_view.setVisibility(View.GONE);
            holder.btn_reject.setVisibility(View.VISIBLE);
            holder.btn_accept.setVisibility(View.VISIBLE);
        }else{
            holder.btn_view.setVisibility(View.VISIBLE);
            holder.btn_reject.setVisibility(View.GONE);
            holder.btn_accept.setVisibility(View.GONE);
        }
        holder.btn_accept.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendStatus("Accept",st.get(position).getFollow_id());
                    }
                }
        );

        holder.btn_reject.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendStatus("Reject",st.get(position).getFollow_id());
                    }
                }
        );

        holder.lytimg.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ProfileActivity.User_id=st.get(position).getId();
                        Intent intent = new Intent(ctx, ProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctx.startActivity(intent);
                    }
                }
        );

        holder.btn_view.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ProfileActivity.User_id=st.get(position).getId();
                        Intent intent = new Intent(ctx, ProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        ctx.startActivity(intent);
                    }
                }
        );
    }

    @Override
    public int getItemCount() {
        return st.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView imgProfile;
        TextView tvName;
        ImageView btn_accept,btn_reject,btn_view;
        LinearLayout lytimg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgProfile=itemView.findViewById(R.id.imgProfile);
            tvName=itemView.findViewById(R.id.tvName);
            btn_accept=itemView.findViewById(R.id.btn_accept);
            btn_reject=itemView.findViewById(R.id.btn_reject);
            lytimg=itemView.findViewById(R.id.lytimg);
            btn_view=itemView.findViewById(R.id.btn_view);
        }
    }
    public void sendStatus(String status,String id){

        ProgressDialog progressDialog=ProgressDialog.show(ctx,"","Loading...");
        GetData getDataServices= RetrofitInstance.getRetrofitInstance().create(GetData.class);
        Call<JsonObject> call=getDataServices.accept("Accept", status,id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JSONObject jsonObject;
                // SharedPrefManager.getInstance(AddAllCategoryActivity.this).userLogin(response.body());
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));

                    String status = jsonObject.getString("res");

                    if (status.equalsIgnoreCase("success")) {
                        Toast.makeText(ctx,"Request "+status+" !",Toast.LENGTH_LONG).show();

                        refresh.onRefresh();
                    }
                    else {
                        Toast.makeText(ctx,"Something Went Wrong !",Toast.LENGTH_LONG).show();

                    }
                }catch(Exception e){
//                    Toast.makeText(AllCategoryActivity.this,e.getMessage().toString().toString(),Toast.LENGTH_LONG).show();

                }
                //    Toast.makeText(AddAllCategoryActivity.this,response.body().toString(),Toast.LENGTH_LONG).show();

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
//                Toast.makeText(AllCategoryActivity.this,t.getMessage().toString(),Toast.LENGTH_LONG).show();

            }
        });
    }

}
