package com.infinitiq.quiz.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.infinitiq.quiz.Adapters.ReuestAdapter;
import com.infinitiq.quiz.R;
import com.infinitiq.quiz.helper.GetData;
import com.infinitiq.quiz.helper.Refresh;
import com.infinitiq.quiz.helper.RetrofitInstance;
import com.infinitiq.quiz.helper.Session;
import com.infinitiq.quiz.model.UserDetails;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyfriendsActivity extends AppCompatActivity implements Refresh {

    Toolbar toolbar;

    TextView txt_request,txt_friends;

    ArrayList<UserDetails> arrayList=new ArrayList<>();
    ArrayList<UserDetails> search=new ArrayList<>();
    RecyclerView requests;

    EditText edit_search;
    int status=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myfriends);

        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("My Friends");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        txt_request=findViewById(R.id.txt_request);
        txt_friends=findViewById(R.id.txt_friend);
        requests=findViewById(R.id.requests);
        edit_search=findViewById(R.id.edit_search);
        changeColor(0);
        txt_request.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status=0;
                        changeColor(0);
                        getRequest();
                    }
                }
        );
        txt_friends.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        status=1;
                        changeColor(1);
                        getMyFriends();
                    }
                }
        );


        edit_search.addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        if(s.length()>0){
                            search.clear();
                            for(int i=0;i<arrayList.size();i++){
                                if(arrayList.get(i).getName().toLowerCase().startsWith(s.toString().toLowerCase())){
                                    search.add(arrayList.get(i));
                                }
                            }
                            if(status==0) {
                                ReuestAdapter orderListAdapter = new ReuestAdapter(search, MyfriendsActivity.this, MyfriendsActivity.this::onRefresh, 0);
                                requests.setAdapter(orderListAdapter);
                            }else{
                                ReuestAdapter orderListAdapter = new ReuestAdapter(search, MyfriendsActivity.this, MyfriendsActivity.this::onRefresh, 1);
                                requests.setAdapter(orderListAdapter);
                            }
                        }else{
                            if(status==0) {
                                ReuestAdapter orderListAdapter = new ReuestAdapter(arrayList, MyfriendsActivity.this, MyfriendsActivity.this::onRefresh, 0);
                                requests.setAdapter(orderListAdapter);
                            }else{
                                ReuestAdapter orderListAdapter = new ReuestAdapter(arrayList, MyfriendsActivity.this, MyfriendsActivity.this::onRefresh, 1);
                                requests.setAdapter(orderListAdapter);
                            }
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                }
        );
        getRequest();
    }

    void changeColor(int i){
        if(i==0){
            txt_request.setBackgroundResource(R.drawable.gradient_cat_blue);
            txt_friends.setBackgroundResource(R.drawable.gradient_cat_gray);
        }else{
            txt_friends.setBackgroundResource(R.drawable.gradient_cat_blue);
            txt_request.setBackgroundResource(R.drawable.gradient_cat_gray);
        }
    }


    public void getRequest(){

        arrayList.clear();
        ProgressDialog progressDialog=ProgressDialog.show(MyfriendsActivity.this,"","Loading...");
        GetData getDataServices= RetrofitInstance.getRetrofitInstance().create(GetData.class);
        Call<JsonObject> call=getDataServices.getRequests("GetRequest", Session.getUserData(Session.USER_ID, MyfriendsActivity.this));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JSONObject jsonObject;
                // SharedPrefManager.getInstance(AddAllCategoryActivity.this).userLogin(response.body());
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));

                    String status = jsonObject.getString("res");

                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            UserDetails categoryModel=new UserDetails(
                                    jsonObject1.getString("id"),
                                    jsonObject1.getString("follow_id"),
                                    jsonObject1.getString("name"),
                                    jsonObject1.getString("profile"),
                                    jsonObject1.getString("status")
                            );
                            arrayList.add(categoryModel);
                        }
                        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
                        requests.setLayoutManager(layoutManager);
                        requests.setHasFixedSize(true);
                        ReuestAdapter orderListAdapter=new ReuestAdapter(arrayList,MyfriendsActivity.this,MyfriendsActivity.this::onRefresh,0);
                        requests.setAdapter(orderListAdapter);

                    }
                    else {
                        ReuestAdapter orderListAdapter=new ReuestAdapter(arrayList,MyfriendsActivity.this,MyfriendsActivity.this::onRefresh,0);
                        requests.setAdapter(orderListAdapter);
//                        Toast.makeText(AllCategoryActivity.this,jsonObject.getString("msg"),Toast.LENGTH_LONG).show();

                    }
                }catch(Exception e){
//                    Toast.makeText(AllCategoryActivity.this,e.getMessage().toString().toString(),Toast.LENGTH_LONG).show();

                }
                //    Toast.makeText(AddAllCategoryActivity.this,response.body().toString(),Toast.LENGTH_LONG).show();

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
              progressDialog.dismiss();
//                Toast.makeText(AllCategoryActivity.this,t.getMessage().toString(),Toast.LENGTH_LONG).show();

            }
        });
    }
    public void getMyFriends(){

        arrayList.clear();
        ProgressDialog progressDialog=ProgressDialog.show(MyfriendsActivity.this,"","Loading...");
        GetData getDataServices= RetrofitInstance.getRetrofitInstance().create(GetData.class);
        Call<JsonObject> call=getDataServices.getRequests("MyFriends", Session.getUserData(Session.USER_ID, MyfriendsActivity.this));
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                JSONObject jsonObject;
                // SharedPrefManager.getInstance(AddAllCategoryActivity.this).userLogin(response.body());
                try {
                    jsonObject = new JSONObject(new Gson().toJson(response.body()));

                    String status = jsonObject.getString("res");

                    if (status.equalsIgnoreCase("success")) {
                        JSONArray jsonArray=jsonObject.getJSONArray("data");

                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            UserDetails categoryModel=new UserDetails(
                                    jsonObject1.getString("id"),
                                    jsonObject1.getString("follow_id"),
                                    jsonObject1.getString("name"),
                                    jsonObject1.getString("profile"),
                                    jsonObject1.getString("status")
                            );
                            arrayList.add(categoryModel);
                        }
                        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getApplicationContext());
                        requests.setLayoutManager(layoutManager);
                        requests.setHasFixedSize(true);
                        ReuestAdapter orderListAdapter=new ReuestAdapter(arrayList,MyfriendsActivity.this,MyfriendsActivity.this::onRefresh,1);
                        requests.setAdapter(orderListAdapter);

                    }
                    else {
                        ReuestAdapter orderListAdapter=new ReuestAdapter(arrayList,MyfriendsActivity.this,MyfriendsActivity.this::onRefresh,1);
                        requests.setAdapter(orderListAdapter);
//                        Toast.makeText(AllCategoryActivity.this,jsonObject.getString("msg"),Toast.LENGTH_LONG).show();

                    }
                }catch(Exception e){
//                    Toast.makeText(AllCategoryActivity.this,e.getMessage().toString().toString(),Toast.LENGTH_LONG).show();

                }
                //    Toast.makeText(AddAllCategoryActivity.this,response.body().toString(),Toast.LENGTH_LONG).show();

                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                progressDialog.dismiss();
//                Toast.makeText(AllCategoryActivity.this,t.getMessage().toString(),Toast.LENGTH_LONG).show();

            }
        });
    }
    @Override
    public void onRefresh() {

        getRequest();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.cate_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

}