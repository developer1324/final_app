package com.infinitiq.quiz.helper;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface GetData {


    @POST("follow.php")
    @FormUrlEncoded
    Call<JsonObject> follow(
            @Field("flag") String flag,
            @Field("user_id") String name,
            @Field("friend_id") String email
    );

    @POST("follow.php")
    @FormUrlEncoded
    Call<JsonObject> getUser(
            @Field("flag") String flag,
            @Field("user_id") String name
    );

    @POST("follow.php")
    @FormUrlEncoded
    Call<JsonObject> getRequests(
            @Field("flag") String flag,
            @Field("user_id") String name
    );

    @POST("follow.php")
    @FormUrlEncoded
    Call<JsonObject> getFriends(
            @Field("flag") String flag,
            @Field("user_id") String name
    );

    @POST("follow.php")
    @FormUrlEncoded
    Call<JsonObject> accept(
            @Field("flag") String flag,
            @Field("status") String statu,
            @Field("follow_id") String id
    );

}
