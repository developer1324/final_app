package com.infinitiq.quiz.helper;

public interface Refresh {
    void onRefresh();
}
